export class Topics {
  constructor(
    {
      name,
      link
    } = {}
  ) {
    this.name = name;
    this.link = link;
  }
}

export const home = new Topics({
  name : "Home",
  link : "/"
});

export const quests = new Topics({
  name : "Guias",
  link : "/quests"
});

export const classes = new Topics({
  name : "Classes",
  link : "/classes"
});
  
export const forges = new Topics({
  name : "Forja",
  link : "/forge"
});
  
export const alchemy = new Topics({
  name : "Alquimia",
  link : "/alchemy"
});

export const history = new Topics({
  name : "História",
  link : "/history"
});

export const donate = new Topics({
  name : "Doar",
  link : "/donate"
});

export const list = [
  quests,
  classes,
  forges,
  alchemy,
  history,
  donate
]