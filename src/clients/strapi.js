import React from "react";
import ApolloClient from 'apollo-boost';

import { Query } from '@apollo/react-components';

export class Strapi {

  static client = new ApolloClient({
    // uri: 'https://cms.ragnamemory.com/graphql',
    uri: 'http://localhost:1337/graphql',
  });

  static query({
    query,
    loading_callback = () => {return "Loading..."},
    error_callback = (error) => {return `Error: ${error.message}`},
    success_callback = (data) => {return `Data: ${JSON.stringify(data)}`}
  } = {})
  {
    return <Query query={query}>
      {
        ({ loading, error, data }) =>
        {
          if (loading) return loading_callback();
          if (error) return error_callback(error);
          return success_callback(data);
        }
      }
    </Query>
  }

}
