import React from 'react';
import './App.css';

import { ApolloProvider } from '@apollo/react-hooks';

import { Strapi } from 'clients/strapi';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import Header from "components/moldures/Header";
import NavBar from "components/moldures/NavBar";
import Footer from 'components/moldures/Footer';

import { home } from "models/Topics";
import { quests } from "models/Topics";
import { donate } from "models/Topics";

import Home from "components/pages/home/Home";
import Donate from "components/pages/donate/Donate";
import Quests from "components/pages/quests/Quests";
import Quest from "components/pages/quests/Quest";

import {Container} from 'react-bootstrap';


function App() {
  return (
    <div className="App">
      <ApolloProvider client={Strapi.client}>
        <Container fluid="true">
          <Router>
            <Header query={Strapi.query}/>
            <NavBar/>
            <Switch>
              <Route exact path={home.link}>
                <Home query={Strapi.query}/>
              </Route>
              <Route
                path={`${quests.link}/:id`}
                component={
                  (props) => <Quest query={Strapi.query} id={props.match.params.id}/>
                }
              />
              <Route path={quests.link}>
                <Quests query={Strapi.query}/>
              </Route>
              <Route path={donate.link}>
                <Donate query={Strapi.query}/>
              </Route>
            </Switch>
            <Footer query={Strapi.query}/>
          </Router>

        </Container>
      </ApolloProvider>
    </div>
  );
}

export default App;
