import './Footer.css'

import React, { Component } from 'react';
import { Card, Nav, Navbar } from 'react-bootstrap';
import ReactHtmlParser from 'react-html-parser';

import { gql } from 'apollo-boost';

const FOOTER = gql`
  {
    pages(where: {reference: "/footer"}) {
      title
      logo {
        url
      }
      content
    }
  }
`;

class Footer extends Component {

  footer({
    body
  } = {}
  ) {
    return (
      <footer className="App-footer">
        <Card bg="dark" style={{ width: '100%' }}>
          <Card.Body className="App-footer">
            {ReactHtmlParser(body)}
          </Card.Body>
        </Card>
        <a style={{ textDecoration: 'none' }} href="#top">
          <Navbar className="App-navbar fixed-bottom neon">
            <Nav className="mr-auto ml-auto" >
              TOPO
            </Nav>
          </Navbar>
        </a>
      </footer>
    );
  }

  render() {
    return this.props.query({
      query: FOOTER,
      success_callback: (data) => {
        data = data.pages[0];
        if(!data)
        {
          return <div>Reload content</div>
        }
        return this.footer({
          body: data.content
        })
      }
    });
  }
}

export default Footer;