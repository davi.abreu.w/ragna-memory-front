import './NavBar.css'

import React from "react";
import { Component } from "react";

import { Nav, Navbar } from 'react-bootstrap';

import { Link } from 'react-router-dom';

import { list as topics } from "models/Topics";

export default class NavBar extends Component {

  render(){
    return (
      <Navbar className="App-navbar" expand="lg" sticky="top">
        <Navbar.Toggle className="bg-gray" aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav"> {
          topics.map(
              topic => 
                <Nav className="mr-auto ml-auto" key={topic.name}>
                  <Link className="neon" to={topic.link} style={{ textDecoration: 'none' }}>
                    {topic.name}
                  </Link>
                </Nav>
            )
        }
        </Navbar.Collapse>
      </Navbar>
    );
  }
}