import './Header.css'

import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import ReactHtmlParser from 'react-html-parser';

import { gql } from 'apollo-boost';

import { home } from "models/Topics";

const HEADER = gql`
  {
    pages(where: {reference: "/header"}) {
      title
      logo {
        url
      }
      content
    }
  }
`;

class Header extends Component {

  header({
    title,
    body
  } = {}
  ) {
    return (
      <Link to={home.link} style={{ textDecoration: 'none' }}>
        <header className="App-header neon">
          <title>{title}</title>
          <Card bg="dark" style={{ width: '100%' }}>
            <Card.Body className="App-header neon">{ReactHtmlParser(body)}</Card.Body>
          </Card>
        </header>
      </Link>
    );
  }

  render() {
    return (
      this.props.query({
        query: HEADER,
        success_callback: (data) => {
          data = data.pages[0];
          if(!data)
          {
            return <div>Reload content</div>
          }
          return this.header({
            title: data.title,
            body: data.content
          })
        }
      })
    );
  }
}

export default Header;