
import React, { Component } from 'react';

import CKEditor from '@ckeditor/ckeditor5-react';
import DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';

export default class Base extends Component {
  render() {
    return (
      <div className="Editor">
        <CKEditor
          editor={ DecoupledEditor }
          data={this.props.data}
          onInit={ editor => {
            editor.isReadOnly = true;
          } }
        />
      </div>
    );
  }
}
