import React from "react";
import { Component } from "react";

import {Card} from 'react-bootstrap';

import Base from './Base'

export default class Quest extends Component {

  card({
    title,
    body
  } = {}
  ) {
    const style = {
      textAlign: 'center'
    }
    return (
      <Card style={style}>
        <Card.Title>{title}</Card.Title>
        <Card.Body>
          <Base data={body}/>
        </Card.Body>
      </Card>
    );
  }

  render() {
    return this.props.query({
      query: this.props.question,
      success_callback: (data) => {
        data = data.quest;
        console.log(data);
        if(!data)
        {
          return <div>Recarregue a página.</div>
        }
        return this.card({
          title: data.Title,
          body: data.Recipe,
        })
      }
    });
  }
}
