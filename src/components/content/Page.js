import React from "react";
import { Component } from "react";

import {Card} from 'react-bootstrap';

import Base from './Base'

export default class Page extends Component {

  card({
    title,
    body,
    image
  } = {}
  ) {
    const style = {
      textAlign: 'center'
    }
    return (
      <Card style={style}>
        { image && <Card.Img variant={this.props.variant} src={image} alt={this.props.alt}/> }
        <Card.Title>{title}</Card.Title>
        <Card.Body>
          <Base data={body}/>
        </Card.Body>
      </Card>
    );
  }

  render() {
    return this.props.query({
      query: this.props.question,
      success_callback: (data) => {
        data = data.pages[0];
        if(!data)
        {
          return <div>Recarregue a página.</div>
        }
        if (data.logo)
        {
          var image = data.logo.url;
        }
        return this.card({
          title: data.title,
          body: data.content,
          image: image
        })
      }
    });
  }
}
