import React from "react";
import { Component } from "react";

import About from "./components/About";
import Carousel from "./components/Carousel";

import { Container, Row, Col } from 'react-bootstrap';

export default class Home extends Component {
  
  render() {
    return (
      <Container className="mt-4 mb-4">
        <Row className="mb-4 justify-content-center">
          <Carousel/>
        </Row>
        <Row className="mb-4 justify-content-center">
          <Col md={6}><About query={this.props.query}/></Col>
        </Row>
      </Container>
    );
  }
}

