import React from "react";
import { Component } from "react";

import {Card} from 'react-bootstrap';

import banner from 'assets/images/carousel/wallpaper6.jpg';

export default class Player extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      title: "Players Online",
      players_number: 5,
      img: banner
    }
  }

  render() {
    const style = {
      textAlign: 'center'
    }
    return (
      <Card style={style}>
        <Card.Img variant="top" src={this.state.img} alt="banner-players"/>
        <Card.Title>{this.state.title}</Card.Title>
        <Card.Subtitle className="text-dark">{this.state.players_number}</Card.Subtitle>
        <Card.Body>
          <Card.Link href="/">Entrar</Card.Link> <Card.Link href="/">Registrar</Card.Link>
        </Card.Body>
      </Card>
    );
  }
}