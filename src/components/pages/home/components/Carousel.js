import './Carousel.css'

import React, { Component } from 'react';

import { Carousel as Cs } from 'react-bootstrap';

import item1 from '../../../../assets/images/carousel/1.jpg';
import item2 from '../../../../assets/images/carousel/2.jpg';
import item3 from '../../../../assets/images/carousel/3.jpg';
import item4 from '../../../../assets/images/carousel/4.jpg';

class Carousel extends Component {

  render() {
    return (
      <Cs className="Carousel">
        <Cs.Item>
          <img
            className="d-block w-100"
            src={item1}
            alt="First slide"
          />
          <Cs.Caption className="Caption">
            <p>Nulla vitae elit libero.</p>
          </Cs.Caption>
        </Cs.Item>
        <Cs.Item>
          <img
            className="d-block w-100"
            src={item2}
            alt="Second slide"
          />
          <Cs.Caption className="Caption">
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Cs.Caption>
        </Cs.Item>
        <Cs.Item>
          <img
            className="d-block w-100"
            src={item3}
            alt="Third slide"
          />
          <Cs.Caption className="Caption">
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
          </Cs.Caption>
        </Cs.Item>
        <Cs.Item>
          <img
            className="d-block w-100"
            src={item4}
            alt="Fourth slide"
          />
        </Cs.Item>
      </Cs>
    );
  }
}

export default Carousel;