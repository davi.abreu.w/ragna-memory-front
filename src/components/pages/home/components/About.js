import React from "react";
import { Component } from "react";

import { gql } from 'apollo-boost';

import Page from 'components/content/Page'

const QUESTION = gql`
  {
    pages(where: {reference: "/about"}) {
      title
      logo {
        url
      }
      content
    }
  }
`;

export default class About extends Component {

  render() {
    return <Page
      query={this.props.query}
      question={QUESTION}
      variant="top"
      alt="about-logo"
    />
  }
}
