import React from "react";
import { Component } from "react";

import {Card} from 'react-bootstrap';

export default class Server extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      title: "Estatísticas do Servidor",
      accounts_number: 1000,
      online_time: "24:00:00"
    }
  }

  render() {
    const style = {
      textAlign: 'center'
    }
    return (
      <Card style={style}>
        <Card.Title>{this.state.title}</Card.Title>
        <Card.Body>
          <p>Contas criadas: {this.state.accounts_number}</p>
          <p>Tempo online: {this.state.online_time}</p>
        </Card.Body>
      </Card>
    );
  }
}