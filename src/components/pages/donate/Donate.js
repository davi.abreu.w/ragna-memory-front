import React from "react";
import { Component } from "react";

import { gql } from 'apollo-boost';

import { Container, Row } from 'react-bootstrap';
import Page from 'components/content/Page'

const QUESTION = gql`
  {
    pages(where: {reference: "/donate"}) {
      title
      logo {
        url
      }
      content
    }
  }
`;

export default class Donate extends Component {

  render() {
    return <Container className="mt-4 mb-4">
      <Row className="mb-4 justify-content-center">
        <Page
          query={this.props.query}
          question={QUESTION}
          variant="top"
          alt="about-logo"
        />
      </Row>
    </Container>
  }
}
