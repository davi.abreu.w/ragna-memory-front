import React from "react";
import { Component } from "react";

import { gql } from 'apollo-boost';

import { Card } from 'react-bootstrap';
import { Container, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

import { quests as root } from "models/Topics";

const QUESTION = gql`
{
	cities{
    id
    Name
    picture {
      url
    }
    quests {
      id
      Title
      episode {
        id
        Title
      }
    }
  }
}
`;

export default class Quests extends Component {

  map_quests(quests) {
    return quests.map(
      quest =>
        <Link to={`${root.link}/${quest.id}`} key={quest.Title} >
          {quest.Title}
        </Link>
        
    )
  }

  render() {
    this.props.query({
      query: QUESTION,
      success_callback: (data) => {
        this.setState({data: data});
      }
    });
    return this.props.query({
      query: QUESTION,
      success_callback: (data) => {
        data = data.cities;
        if(!data)
        {
          return <div>Recarregue a página.</div>
        }
        return (
          <Container className="mt-4 mb-4">
          {
            data.map(
              city => 
                Array.isArray(city.quests) && !!city.quests.length &&
                <Row className="mb-4 justify-content-center" key={city.Name}>
                  <Col md={6}>
                    <Card>
                      { (city.picture &&
                        <Card.Img
                          src={city.picture.url}
                          style={{
                            maxWidth: "50%"
                          }}
                        />) ||
                        <Card.Title>
                          {city.Name}
                        </Card.Title>
                      }
                      <Card.Body>
                          {this.map_quests(city.quests)}
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
            ) 
          }
          </Container>
        )
      }
    });
  }
}

