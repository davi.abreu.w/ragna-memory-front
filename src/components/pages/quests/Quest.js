import React from "react";
import { Component } from "react";

import { gql } from 'apollo-boost';

import { default as Content } from 'components/content/Quest'


export default class Quest extends Component {

  render() {
    let id = this.props.id;

    let question = gql`
    {
      quest(id: ${id}) {
        Title
        Recipe
      }
    }`;

    return <Content
      query={this.props.query}
      question={question}
      variant="top"
      alt="about-logo"
    />
  }
}
